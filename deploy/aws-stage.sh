#!/bin/sh

cd ..
folderName=`basename "$PWD"`

set -e -x
rm -fR node_modules
cd ..
tar cfz promo-portal-be.tar.gz ${folderName}
ssh -o StrictHostKeyChecking=no -i ~/.ssh/promo-portal-back-end.pem ec2-user@34.248.248.189 'killall screen'
ssh -o StrictHostKeyChecking=no -i ~/.ssh/promo-portal-back-end.pem ec2-user@34.248.248.189 'rm -fR ~/promo-portal-be'
scp -i ~/.ssh/promo-portal-back-end.pem ./promo-portal-be.tar.gz ec2-user@34.248.248.189:~/
cd ~/.ssh
ssh -o StrictHostKeyChecking=no -i promo-portal-back-end.pem ec2-user@34.248.248.189 'tar xfz ./promo-portal-be.tar.gz'
ssh -o StrictHostKeyChecking=no -i promo-portal-back-end.pem ec2-user@34.248.248.189 'cd ./promo-portal-be && npm install'
ssh -o StrictHostKeyChecking=no -i promo-portal-back-end.pem ec2-user@34.248.248.189 'cd ./promo-portal-be && screen -d -m npm start'
