# Docker / Container #

## Build ##

From the root directory of the repo run:

    ./docker/container/build.sh

## Run ##

From the root directory of the repo run:

    ./docker/container/run.sh
    
After use this commands you need wait couple minutes to allow the program to finish serving. 
Then you can go to http://localhost:4000/graphql and use the application.

## Stop ##

From the root directory of the repo run:

    ./docker/container/stop.sh

## Remove ##

From the root directory of the repo run:

    ./docker/container/remove.sh

## Connect to the container ##

From the root directory of the repo run:

    ./docker/container/exec.sh
