# Promo Portal GraphQL mock be

## query examples

```
query getCategoryList {
  getCategories{
    id
    name
    icons{
      url
      height
      width
    }
  }
}

query userLogin($username: String!, $password: String!) {
  login(username: $username, password: $password) {
    id
    email
    username
  }
}

query userLogout($username: String!) {
  logout(username: $username)
}

query headerCounts {
  getHeaderCounts {
    inboxItemsCount
    offlineItemsCount
    favoriteItemsCount
  }
}

query getAvailableUsers($offset: Int, $limit: Int) {
  getUsers(offset: $offset, limit: $limit) {
    items {
      id
      username
    }
    limit
    offset
    total
  }
}
```
