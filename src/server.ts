import express from 'express';
import bodyParser = require('body-parser');
import AWS = require('aws-sdk');
import { createServer } from 'http';
import { PubSub } from 'apollo-server';
import { ApolloServer, gql } from 'apollo-server-express';
import { SendMessagesRequest } from 'aws-sdk/clients/pinpoint';
import moment from 'moment';
import cors = require('cors');

const pubsub = new PubSub();

const link = 'http://promo-portal-alb-1560658871.eu-west-1.elb.amazonaws.com/auth/login';

// @ts-ignore
const pgp = require('pg-promise')();
// @ts-ignore
const db = pgp('postgres://mattcarp:portal-24rcdto@portal-test-db.cjv01xmpichu.eu-west-1.rds.amazonaws.com:5432/portal_test_db');

// GraphQL Schema
const typeDefs = gql`
  type Subscription {
    categoryUpdated: [CategoryItem]
    userLoginStatusUpdated: Boolean
  }
  type Query {
    getCategories: [CategoryItem]
    getCategoryImages(categoryName: String!): [String]
    # auth
    forgotPassword(username: String!): String
    # delivery
    getDeliveryItem(id: ID!): DeliveryItem
    getDeliveryItemRequestPasscode(id: ID!): MessageItem
    getDeliveryItemAssets(id: ID!, offset: Int, limit: Int): DeliveryItemAssets
    getDistributionDetailsItem(id: ID!): DistributionDetailsItem
    # favorites
    getFavoriteItemsCount: ItemsCount
    getFavoriteItems(offset: Int, limit: Int): CategoryItems
    # header
    getHeaderCounts: HeaderCounts
    # inbox
    getInboxItemsCount: ItemsCount
    getInboxItems: CategoryItems
    # offlineItems
    getOfflineItemsCount: ItemsCount
    # users
    getUsers(offset: Int, limit: Int): UserItems
    getCurrentUser(id: ID!): User
    getUserLoginStatus(id: ID!): Boolean
  }

  type Mutation {
    # delivery
    validateDeliveryItemPasscode(id: ID!, passcode: String!): MessageItem
    # favorites
    updateFavoriteItem(id: ID!, name: String!): CategoryItems
    # inbox
    updateInboxItems(id: [ID]): CategoryItems
    # auth
    login(email: String!, password: String!): User
    logout(id: ID!): MessageItem
    # users
    updateUser(id: ID!, name: String, email: String, mobileNumber: String): User
    createUser(name: String!, username: String!, email: String!,
                mobileNumber: String!, password: String!): MessageItem
    deleteUser(id: ID!): MessageItem
    # categories
    updateCategoryItem(id: String!, name: String): MessageItem
  }

  type DeliveryItem {
    id: ID
    name: String
    assets: [DeliveryAssetItem]
    distributionDetails: Int
  }

  type MessageItem {
    message: String
  }

  type DeliveryItemAssets {
    items: [DeliveryAssetItem],
    limit: Int
    offset: Int
    page: Int
    total: Int
  }

  type DeliveryAssetItem {
    id: ID
    name: String
    description: String
  }

  type DistributionDetailsItem {
    id: ID
    name: String
    description: String
    label: String
    message: String
  }

  type CategoryItems {
    href: String
    items: [CategoryItem]
    limit: Int
    next: String
    offset: Int
    previous: String
    total: Int
  }

  type CategoryItem {
    href: String
    icons: [IconItem]
    id: String
    name: String
  }

  type IconItem {
    height: Int
    url: String
    width: Int
  }

  type ItemsCount {
    count: Int
  }

  type HeaderCounts {
    favoriteItemsCount: Int
    inboxItemsCount: Int
    offlineItemsCount: Int
  }

  type UserItems {
    items: [User]
    limit: Int
    offset: Int
    page: Int
    total: Int
  }

  type User {
    id: ID
    name: String
    username: String
    email: String
    password: String
    oneTimePassword: String
    mobileNumber: String
    last_login_timestamp: String
  }
`;

// events
const EVENTS = {
  CATEGORY_UPDATED: 'categoryUpdated'
};

// currently are in use
const getCategoryImages = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.any(`select href from category_images where category_name='${params.categoryName}'`)
      .then((data: any) => {
        resolve(data.map((item: any) => item.href)
          .map((item: any) => ({ url: item, width: 100, height: 100 })));
      })
      .catch((error: any) => reject(error));
  });
};

const getCategories = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.any('select * from categories order by name asc')
      .then((data: any) => resolve(data))
      .catch((error: any) => reject(error));
  });
};

const updateCategoryItem = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.any(`update categories set name = '${params.name}' where id = '${params.id}'`)
      .then(() => {
        pubsub.publish(EVENTS.CATEGORY_UPDATED, { categoryUpdated: getCategories });
        resolve({ message: 'Data has been successfully updated' });
      })
      .catch((error: any) => reject(error));
  });
};

// auth
const getUserLoginStatus = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve) => {
    if (!params.id || Number(params.id) === 0) {
      resolve(false);
      return;
    }
    db.one(`select * from users where id=${params.id}`)
      .then((user: any) => {
        const timestamp = (user && user.last_login_timestamp) ? moment(user.last_login_timestamp) : null;
        const isExpired = (!timestamp || (timestamp.diff(moment(), 'days') >= 1));
        resolve(!isExpired);
      })
      .catch(() => resolve(false));
  });
};

const login = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.one(`select * from users where email='${params.email}'`)
      .then((user: any) => {
        if (user && user.password === params.password) {
          const timestamp = moment().format('YYYY-MM-DD HH:MM:SS');
          db.none(`update users set last_login_timestamp = '${timestamp}' where id = '${user.id}'`)
            .then(() => {
              resolve(user);
            })
            .catch((error: any) => reject(error));
        } else {
          reject('Wrong password or email');
        }
      })
      .catch((error: any) => reject(error));
  });
};

const logout = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.one(`select * from users where id = '${params.id}'`)
      .then((user: any) => {
        db.none(`update users set last_login_timestamp = '' where id = '${user.id}'`)
          .then(() => {
            resolve({ message: 'User has been logged out' });
          })
          .catch((error: any) => reject(error));
      })
      .catch((error: any) => reject(error));
  });
};

const forgotPassword = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.one(`select * from users where username='${params.username}'`)
      .then((user: any) => resolve('Link for password recovery'))
      .catch((error: any) => reject(error));
  });
};

// delivery
const getDeliveryItem = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select * from deliveries where id = ${params.id}`;
    db.one(query)
      .then((item: any) => resolve(item))
      .catch((error: any) => reject(error));
  });
};

const getDeliveryItemRequestPasscode = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select passcode from deliveries where id = ${params.id}`;
    db.one(query)
      .then((item: any) => resolve({ message: item.passcode }))
      .catch((error: any) => reject(error));
  });
};

const validateDeliveryItemPasscode = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select passcode from deliveries where id = ${params.id}`;
    db.one(query)
      .then((item: any) => {
        const passcode = params.passcode;
        const passcodeIsValid = item && passcode && passcode.length > 3;
        if (passcodeIsValid) {
          const updateQuery = `update users set passcode = '${passcode}' where id = ${item.id}`;
          db.one(updateQuery)
            .then(() => resolve({ message: 'Passcode has been successfully updated' }))
            .catch((error: any) => reject(error));
        }
      })
      .catch((error: any) => reject(error));
  });
};

const getDeliveryItemAssets = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select assets from deliveries where id = ${params.id}`;
    db.one(query)
      .then((item: any) => {
        let offset = params && params.offset;
        let limit = params && params.limit;
        let assetsQuery = `select * from assets where id in (${item.assets})`;
        limit ? assetsQuery += ` limit ${limit}` : limit = 0;
        offset ? assetsQuery += ` offset ${offset}` : offset = 0;
        db.any(assetsQuery)
          .then((items: any[]) => {
            if (!limit) {
              limit = items.length;
            }
            resolve({
              items,
              limit,
              offset,
              page: limit ? Math.floor(items.length / limit) : 1,
              total: items.length
            });
          })
          .catch((error: any) => reject(error));
      })
      .catch((error: any) => reject(error));
  });
};

const getDistributionDetailsItem = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select * from distribution_details where id = ${params.id}`;
    db.one(query)
      .then((item: any) => resolve(item))
      .catch((error: any) => reject(error));
  });
};

// favorite items
const getFavoriteItemsCount = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select * from favorite_items`;
    db.any(query)
      .then((items: any[]) => resolve({ count: items.length }))
      .catch((error: any) => reject(error));
  });
};

const getFavoriteItems = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    let offset = params && params.offset;
    let limit = params && params.limit;
    let query = `select * from favorite_items`;
    limit ? query += ` limit ${limit}` : limit = 0;
    offset ? query += ` offset ${offset}` : offset = 0;
    db.any(query)
      .then((items: any[]) => {
        if (!limit) {
          limit = items.length;
        }
        resolve(
          {
            href: '',
            items,
            limit,
            next: '',
            previous: '',
            offset,
            total: items.length
          }
        );
      })
      .catch((error: any) => reject(error));
  });
};

const updateFavoriteItem = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `select * from favorite_items where id = ${params.id}`;
    db.one(query)
      .then((item: any) => {
        const updateQuery = `update favorite_items set name = '${params.name}' where id = ${item.id}`;
        db.one(updateQuery)
          .then(() => getFavoriteItems(root, params, context))
          .catch((error: any) => reject(error));
      })
      .catch((error: any) => reject(error));
  });
};

// header
const getHeaderCounts = (root: any, params: any, context: any = {}) => {
  return {
    favoriteItemsCount: 7,
    inboxItemsCount: 8,
    offlineItemsCount: 9
  };
};

// inbox
const getInboxItemsCount = (root: any, params: any, context: any = {}) => {
  return { count: 77 };
};

const getInboxItems = (root: any, params: any, context: any = {}) => {
  return getCategories({}, { name: 'workout' }, {});
};

const updateInboxItems = (root: any, params: any, context: any = {}) => {
  // TODO: update ids
  return getInboxItems;
};

// offline items
const getOfflineItemsCount = (root: any, params: any, context: any = {}) => {
  return { count: 7 };
};

// users
const getUsers = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    let offset = params && params.offset;
    let limit = params && params.limit;
    let query = `select * from users`;
    limit ? query += ` limit ${limit}` : limit = 0;
    offset ? query += ` offset ${offset}` : offset = 0;
    db.any(query)
      .then((items: any[]) => {
        if (!limit) {
          limit = items.length;
        }
        resolve(
          {
            items,
            limit,
            offset,
            page: limit ? Math.floor(items.length / limit) : 1,
            total: items.length
          }
        );
      })
      .catch((error: any) => reject(error));
  });
};

const updateUser = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.one(`select * from users where id=${params.id}`)
      .then((user: any) => {
        const query = `update users
          set name = '${params.name || user.name}',
              email = '${params.email || user.email}',
              mobileNumber = '${params.mobileNumber || user.mobileNumber}'
          where
            id = ${user.id}`;
        db.one(query)
          .then((resp: any) => resolve(resp))
          .catch((error: any) => reject(error));
      })
      .catch((error: any) => reject(error));
  });
};

const createUser = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `insert into users (name, username, email, password, mobileNumber, onetimepassword)
      values ('${params.name}', '${params.username}', '${params.email}', '${params.password}',
      '${params.mobileNumber}', '')`;
    db.none(query)
      .then(() => resolve({ message: 'Data has been successfully updated' }))
      .catch((error: any) => reject(error));
  });
};

const deleteUser = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    const query = `delete from users where id=${params.id}`;
    db.none(query)
      .then(() => resolve({ message: 'Data has been successfully deleted' }))
      .catch((error: any) => reject(error));
  });
};

const getCurrentUser = (root: any, params: any, context: any = {}) => {
  return new Promise((resolve, reject) => {
    db.one(`select * from users where id=${params.id}`)
      .then((user: any) => {
        resolve(user);
      })
      .catch((error: any) => reject(error));
  });
};

// Root resolver
const resolvers = {
  Query: {
    getCategories,
    getCategoryImages,
    forgotPassword,
    getDeliveryItem,
    getDeliveryItemRequestPasscode,
    getDeliveryItemAssets,
    getDistributionDetailsItem,
    getFavoriteItemsCount,
    getFavoriteItems,
    getHeaderCounts,
    getInboxItemsCount,
    getInboxItems,
    getOfflineItemsCount,
    getUsers,
    getCurrentUser,
    getUserLoginStatus
  },
  Mutation: {
    login,
    logout,
    validateDeliveryItemPasscode,
    updateFavoriteItem,
    updateInboxItems,
    updateUser,
    createUser,
    deleteUser,
    updateCategoryItem
  },
  Subscription: {
    categoryUpdated: {
      subscribe: () => pubsub.asyncIterator(EVENTS.CATEGORY_UPDATED)
    }
  }
};

const app = express();
const appRest = express();

const server = new ApolloServer({ typeDefs, resolvers });
server.applyMiddleware({ app, path: '/graphql' });

const httpServer = createServer(app);
server.installSubscriptionHandlers(httpServer);

AWS.config.region = 'us-east-1';
AWS.config.update({
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-1:5b169af9-a481-41af-94c8-bed5d098a145'
  })
});

const pinpoint = new AWS.Pinpoint({apiVersion: '2016-12-01'});
const paramsAWS: SendMessagesRequest = {
  ApplicationId: '2ace37a50e074bb6af81d660652dc661',
  MessageRequest: {
    Context: {},
    Addresses: {},
    MessageConfiguration: {
      EmailMessage: {
        SimpleEmail: {
          Subject: {
            Charset: 'UTF-8',
            Data: 'Restore password'
          },
          TextPart: {}
        }
      }
    }
  }
};

httpServer.listen({ port: 4000 }, () => {
  console.log('Apollo Server on http://localhost:4000/graphql');
});

appRest.use(bodyParser.urlencoded({extended: false}));
appRest.use(bodyParser.json());
appRest.use(cors());
appRest.options('*', cors());

appRest.post('/pum/v1/users/forgotPassword', (req: any, res: any, next: any) => {
  if (!req || !req.body || !req.body.emailAddress) {
    res.statusCode = 400;
    const e = new Error('Request body is empty');
    next(e);
    return;
  }
  const email = req.body.emailAddress;
  paramsAWS.MessageRequest.Addresses = {[email]: {ChannelType: 'EMAIL'}};
  paramsAWS.MessageRequest
    .MessageConfiguration
    .EmailMessage
    .SimpleEmail
    .TextPart = {
    Charset: 'UTF-8',
    Data: `To restore your password please follow the link: ${link}`
  };
  pinpoint.sendMessages(paramsAWS, function (err: any, data: any) {
    if (err) {
      next(err);
    } else {
      res.send(data);
    }
  });
});


appRest.listen({port: 3000}, () => {
  console.log(`REST Server running at http://localhost:3000/`);
});
